variable "huit_assetid" {}

variable "product" {}

variable "environment" {}

variable "environment_map" {
  type    = "map"
  default = {
    dev   = "Development"
    test  = "Testing"
    stage = "Staging"
    prod  = "Production"
  }
}

# Required Data Providers
data "aws_availability_zones" "available" {}

# AWS Infrastructure Info
variable "vpc_id" {}

# AWS EFS Information
# List of AWS EFS Subnets
variable "efs_subnets" {
  type = "list"
}

variable "app_scope" {
  description = "Function string to be incorporated into naming"
  default  = "app"
}

# List of allowed security groups which can mount the EFS filesystem
variable "efs_source_sg" {
}

variable "efs_performance_mode" {
  default = "generalPurpose"
}

# Adding to allow for module dependancy - see https://github.com/hashicorp/terraform/issues/1178#issuecomment-224782073
variable "wait_on" {
  description = "Variable to hold outputs of other moudles to force waiting"
  default     = "Nothing"
}
