output "efs_sg_id" {value = "${aws_security_group.efs-sg.id}"}
output "efs_fs_id" {value = "${aws_efs_file_system.efs-fs.id}"}
output "efs_mount_target_ids" {value = "${join(",",aws_efs_mount_target.efs-mount-targets.*.id)}"}
output "efs_mount_ips" {value = "${join(",", aws_efs_mount_target.efs-mount-targets.*.ip_address)}"}