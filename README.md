# EFS Terraform Module

## Module Overview

The module sets a EFS file system and associated mount targets


## Module Parameters 

| Parameter             | Type   | Required? | Description                                                                                                               |
|:-----------------------:|:--------:|:-----------:|---------------------------------------------------------------------------------------------------------------------------|
| huit_assetid          | String |     Y     | 4 Digit Asset ID assigned by SLASH                                                                                        |
| product               | String |     Y     | Application name as it appears in SNOW                                                                                    |
| environment           | String |     Y     | Environment of the application - One of (dev, test, stage, prod)                                                         |
| app_scope             | String |     Y     | Function string to be incorporated into naming                                                                           |
| vpc_id                | String |     Y     | VPC ID for the ELB                                                                                                        |
| efs_subnets           | List   |     Y     | List of subnets for the EFS Mount Targets to reside in                                                                                  |
| efs_source_sg         | List   |     Y     | List of security group ID's which can access the EFS File system                                   |
| efs_performance_mode  | String |     N     | Performance mode for the EFS file system, defaults to generalPurpose                                   |


## Module Outputs

| Output Name | Type   | Description                           |
|:-----------:|--------|---------------------------------------|
| efs_fs_id      | String | AWS ID of the EFS File System                    |
| efs_mount_target_ids    | List | List of the EFS Mount Target ID's                |
| efs_sg_id   | String | Security Group ID created for the EFS Mount Targets |
| efs_mount_ips    | List | List of the EFS Mount Target IP Addresses                    |
