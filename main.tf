# EFS Filesystem Creation
resource "aws_efs_file_system" "efs-fs" {
  creation_token = "${lower(var.product)}-${var.environment}-${var.app_scope}-efs"
  tags {
    Name         = "${lower(var.product)}-${var.environment}-${var.app_scope}-efs"
    product      = "${var.product}"
    huit_assetid = "${var.huit_assetid}"
    environment  = "${lookup(var.environment_map, var.environment)}"
  }
}

# EFS Security Group for Mount Targets
resource "aws_security_group" "efs-sg" {
  name_prefix = "${lower(var.product)}-${var.environment}-${var.app_scope}-efs-sg-"
  description = "Allow EFS in ${var.vpc_id} to ${aws_efs_file_system.efs-fs.tags.Name}"
  vpc_id      = "${var.vpc_id}"

  ingress {
    from_port       = 2049
    to_port         = 2049
    protocol        = "tcp"
    security_groups = ["${var.efs_source_sg}"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name         = "${lower(var.product)}-${var.environment}-${var.app_scope}-efs-sg"
    environment  = "${lookup(var.environment_map, var.environment)}"
    product      = "${var.product}"
    huit_assetid = "${var.huit_assetid}"
  }
}

# Creation of EFS Mount Target
resource "aws_efs_mount_target" "efs-mount-targets" {
  file_system_id  = "${aws_efs_file_system.efs-fs.id}"
  subnet_id       = "${element(var.efs_subnets, count.index)}"
  count           = "${length(var.efs_subnets)}"
  security_groups = ["${aws_security_group.efs-sg.id}"]

}

# Allows for module dependancy chains, if not passed, not used
# Requires top dependancy of module to have: depends_on = ["null_resource.wait_on"]
resource "null_resource" "wait_on" {
  provisioner "local-exec" {
    command = "echo Waited on ${var.wait_on} before proceeding"
  }
}
